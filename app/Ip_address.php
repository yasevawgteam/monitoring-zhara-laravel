<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ip_address extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "ip_address";
}
