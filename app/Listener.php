<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listener extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "listeners";
}
