<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation_system extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "operation_system";
}
