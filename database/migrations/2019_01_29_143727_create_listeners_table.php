<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListenersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listeners', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('time_start');
            $table->integer('time_stop');

            $table->integer('time_listen');

            $table->unsignedInteger('city_id');
            $table->foreign('city_id')->references('id')->on('city');

            $table->unsignedInteger('device_id');
            $table->foreign('device_id')->references('id')->on('device');

            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listeners');
    }
}
